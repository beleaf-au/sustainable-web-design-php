<?php

require_once '../vendor/autoload.php';

use Beleaf\SustainableWebDesign\Model\V4 as SWD;

$results = SWD::url('https://beleaf.au', [
    'gpsapi' => [
        'strategy' => 'desktop',
        'key' => '',
    ],
]);

print_r($results);
