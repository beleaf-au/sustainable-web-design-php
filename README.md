
# Sustainable Web Design model for PHP

A library for estimating the carbon emissions from using digital services using the [Sustainable Web Design model](https://sustainablewebdesign.org/).

## Installation

To begin, you'll need to add the library to your `composer.json`

```shell
composer require beleaf/sustainable-web-design
```

After adding the library, update your packages using `composer update` or install them using `composer install`.

## Usage

```php
require_once './vendor/autoload.php';

use \Beleaf\SustainableWebDesign\SustainableWebDesign as SWD;

$results = SWD::url('https://beleaf.au');

print_r($results);
```

### Use a different model
You can instruct the SWD test method to estimate the emissions using a different model by providing the model number as the third parameter

```php
require_once './vendor/autoload.php';

use \Beleaf\SustainableWebDesign\SustainableWebDesign as SWD;

$results = SWD::url('https://beleaf.au', [], 3);

print_r($results);
```

Alternatively, you can access the test method directly on the model and avoid the SWD wrapper

```php
require_once './vendor/autoload.php';

use \Beleaf\SustainableWebDesign\Model\V3 as SWDv3;

$results = SWDv3::url('https://beleaf.au', []);

print_r($results);
```

## SWD wrapper and Model methods

The SWD wrapper, and each of the SWD models expose 3 methods that can be called to estimate emissions. The models may choose to expose additional methods, but these are not consistent from model to model.

Each of the methods accept atleast 2 parameters, with the SWD wrapper methods accepting a third (integer) representing the model to use. The SWD wrapper defaults to the latest model released.

Each of the models have different formulas for estimating emissions, and each model has different parameters that can be overwritten. If you know the carbon intensity of the electricity grid in which your data center, network, or user devices are located, you should adjust the grid intensity to estimate the operational emissions of that segment. This will give you results that are more representative of how your website is operated and used.

### SWD::test

Test will return the estimated emissions for a page view of the url provided. Test requires atleast the url to be tested as the first parameter, and accepts a second parameter allowing for configuration of the test, and the values provided to the formulas.

The test method will make a call to the GPSAPI to calculate the quantity of bytes transferred. The GPSAPI defaults to testing a desktop screens size, but this can be changed to mobile. Additionally, the key can be provided to allow more frequent tests. A key can be acquired from [Google](https://developers.google.com/speed/docs/insights/v5/get-started).

The test method will also make a call to then Green Web Foundation API's to retrieve energy and emissions values associated with the url provided.

```php
require_once './vendor/autoload.php';

use \Beleaf\SustainableWebDesign\SustainableWebDesign as SWD;

$results = SWD::url('https://beleaf.au', [
    'gpsapi' => [
        'strategy' => 'mobile',
        'key' => 'xxx',
    ],
]);

print_r($results);
```

#### Response

The test method returns an array from the Model::breakdown method. See [SWD::breakdown response](#swd-breakdown-response) for the structure.

### SWD::breakdown

Breakdown returns an array containing the estimated emissions, the rating (if available on the model - V3 onwards), the estimated emissions for each segment in the model, and the variables utilised by the formulas.

SWD::breakdown accepts two parameters, a int|float representing the bytes transferred, and an array of parameters for configuring the values used in the forumlas.

```php
require_once './vendor/autoload.php';

use \Beleaf\SustainableWebDesign\SustainableWebDesign as SWD;

$results = SWD::breakdown(153735);

print_r($results);
```

#### <a name="swd-breakdown-response"></a>Response

Note: The structure of the reponse differs from model to model.

```php
Array
(
    [gCO2e] => 0.024419489257414
    [rating] => A+
    [segments] => Array
        (
            [operationalTotal] => 0.014765805164566
            [embodiedTotal] => 0.010680135888979
            [operational] => Array
                (
                    [dataCentre] => 0.0041861818765523
                    [network] => 0.0044906314675743
                    [userDevice] => 0.0060889918204397
                )
            [embodied] => Array
                (
                    [dataCentre] => 0.00084875252097845
                    [network] => 0.004173033228144
                    [userDevice] => 0.0056583501398563
                )
        )
    [variables] => Array
        (
            [operationalEnergyIntensity] => 531.597
            [embodiedEnergyIntensity] => 494
            [greenHostingFactor] => 0.2452
            [dataCacheRatio] => 0
            [returnVisitorRatio] => 0
            [gpsapi] => Array
                (
                    [strategy] => desktop
                    [key] => 
                )
            [bytes] => 153735
        )
)
```

### SWD::gCO2e

gCO2e returns a float estimating the emissions.

gCO2e accepts two parameters, a int|float representing the bytes transferred, and an array of parameters for configuring the values used in the forumlas.

```php
require_once './vendor/autoload.php';

use \Beleaf\SustainableWebDesign\SustainableWebDesign as SWD;

$results = SWD::gCO2e(153735);

print_r($results);
```

#### Response

```php
0.024419489257414
```

## License
Sustainable Web Design model for PHP is licensed under the MIT License - see the LICENSE file for details.