<?php

namespace Beleaf\SustainableWebDesign;

class SustainableWebDesign
{
    public const LATEST_MODEL = 4;

    public static function url(string $url, array $attributes = [], int $model = self::LATEST_MODEL): array
    {
        $class = "\Beleaf\SustainableWebDesign\Model\V$model";
        return $class::url($url, $attributes);
    }

    public static function urlWithBytes(string $url, int $bytes, array $attributes = [], int $model = self::LATEST_MODEL): array
    {
        $class = "\Beleaf\SustainableWebDesign\Model\V$model";
        return $class::urlWithBytes($url, $bytes, $attributes);
    }

    public static function breakdown(float $bytes, array $attributes = [], int $model = self::LATEST_MODEL): array
    {
        $class = "\Beleaf\SustainableWebDesign\Model\V$model";
        return $class::breakdown($bytes, $attributes);
    }

    public static function gCO2e(float $bytes, array $attributes = [], int $model = self::LATEST_MODEL): float
    {
        $class = "\Beleaf\SustainableWebDesign\Model\V$model";
        return $class::gCO2e($bytes, $attributes);
    }
}
