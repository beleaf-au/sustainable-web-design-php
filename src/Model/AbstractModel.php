<?php

namespace Beleaf\SustainableWebDesign\Model;

abstract class AbstractModel
{
    abstract public static function url(string $url, array $attributes = []): array;
    abstract public static function urlWithBytes(string $url, int $bytes, array $attributes = []): array;
    abstract public static function breakdown(float $bytes, array $attributes = []): array;
    abstract public static function gCO2e(float $bytes, array $attributes = []): float;

    // V1 and V2 multiply and divide the bytes transfered resulting in a float.
    protected static function bytesToGB(float $bytes): float
    {
        return $bytes / pow(1024, 3);
    }

    protected static function bytesFromGPSAPI(string $url, array $attributes = []): ?int
    {
        // https://developers.google.com/speed/docs/insights/rest/v5/pagespeedapi/runpagespeed
        $params = [
            'strategy' => $attributes['gpsapi']['strategy'] ?? 'desktop',
            'url' => $url,
        ];

        if (!empty($attributes['gpsapi']['key'])) {
            $params['key'] = $attributes['gpsapi']['key'];
        }

        $json = self::requestAsJson('https://www.googleapis.com/pagespeedonline/v5/runPagespeed?' . http_build_query($params));

        // Return either the transfer size of null
        return $json->lighthouseResult->audits->{'total-byte-weight'}->numericValue ?? null;
    }

    protected static function ipToCO2Intensity(string $url): ?\stdClass
    {
        return self::requestAsJson('https://api.thegreenwebfoundation.org/api/v3/ip-to-co2intensity/' . self::extractHost($url));
    }

    protected static function greenCheck(string $url): ?\stdClass
    {
        return self::requestAsJson('https://api.thegreenwebfoundation.org/greencheck/' . self::extractHost($url));
    }

    private static function extractHost(string $url): string
    {
        $host = parse_url($url)['host'] ?? null;

        if ($host === null) {
            throw new \Exception("Host cannot be extracted from $url");
        }

        return $host;
    }

    private static function requestAsJson(string $url): ?\stdClass
    {
        return json_decode(self::request($url)) ?? null;
    }

    private static function request(string $url): string
    {
        return file_get_contents($url);
    }
}
