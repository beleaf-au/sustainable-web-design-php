<?php

namespace Beleaf\SustainableWebDesign\Model;

class V2 extends AbstractModel
{
    public const KWH_PER_GB = 1.805;
    public const GLOBAL_AVERAGE_CO2_INTENSITY_GRID = 475;

    public static function url(string $url, array $attributes = []): array
    {
        // Only get the an interngy intensity from GWF if one is not provided
        if(empty($attributes['green'])) {
            $attributes['green'] = self::green($url);
        }

        // Process the attributes to get the defaults
        $attributes = self::attributes($attributes);

        return self::breakdown(
            self::bytesFromGPSAPI($url, $attributes),
            $attributes,
        );
    }

    public static function urlWithBytes(string $url, int $bytes, array $attributes = []): array
    {
        // Only get the an interngy intensity from GWF if one is not provided
        if (empty($attributes['green'])) {
            $attributes['green'] = self::green($url);
        }

        // Process the attributes to get the defaults
        $attributes = self::attributes($attributes);

        return self::breakdown(
            $bytes,
            $attributes,
        );
    }

    public static function breakdown(float $bytes, array $attributes = []): array
    {
        $attributes = self::attributes($attributes);

        $gCO2e = self::gCO2e($bytes, $attributes);
        $energy = self::energy($bytes);

        $attributes['bytes'] = $bytes;
        $attributes['adjustedBytes'] = self::adjustDataTransfer($bytes);

        return [
            'gCO2e' => $gCO2e,
            'energy' => $energy,
            'variables' => $attributes,
        ];
    }

    public static function gCO2e(float $bytes, array $attributes = []): float
    {
        $attributes = self::attributes($attributes);

        $energy = $attributes['adjustBytesTransfered']
            ? self::energy(self::adjustDataTransfer($bytes))
            : self::energy($bytes);

        // Need to find out what the magic numbers represent
        return $attributes['green']
            ? (($energy * 0.1008) * 33.4) + (($energy * 0.8992) * self::GLOBAL_AVERAGE_CO2_INTENSITY_GRID)
            : $energy * self::GLOBAL_AVERAGE_CO2_INTENSITY_GRID;
    }

    private static function adjustDataTransfer(float $bytes): float
    {
        $newVisitorPercentage = 0.75;
        $oldVisitorPercentage = 1 - $newVisitorPercentage;
        $percentageLoadedOnSubsequentLoad = 0.02;

        return ($bytes * $newVisitorPercentage) + ($percentageLoadedOnSubsequentLoad * $bytes * $oldVisitorPercentage);
    }

    private static function energy(float $bytes): float
    {
        return self::bytesToGB($bytes) * self::KWH_PER_GB;
    }

    private static function green(string $url): float
    {
        $data = self::greenCheck($url);

        return property_exists($data, 'green') && $data->green === true;
    }

    private static function attributes(array $attributes = []): array
    {
        return array_merge([
            'adjustBytesTransfered' => true,
            'green' => false,
        ], $attributes);
    }
}
