<?php

namespace Beleaf\SustainableWebDesign\Model;

// https://sustainablewebdesign.org/estimating-digital-emissions-version-3/

class V3 extends AbstractModel
{
    public const KWH_PER_GB = 0.81;
    public const GLOBAL_AVERAGE_CO2_INTENSITY_GRID = 442;
    public const GLOBAL_AVERAGE_CO2_INTENSITY_RENEWABLE = 50;
    public const PERCENTAGE_OF_ENERGY_IN_DATACENTER = 0.15;
    public const PERCENTAGE_OF_ENERGY_IN_TRANSMISSION = 0.14;
    public const PERCENTAGE_OF_ENERGY_IN_END_USER_DEVICES = 0.52;
    public const PERCENTAGE_OF_ENERGY_IN_HARDWARE_PRODUCTION = 0.19;

    public static function url(string $url, array $attributes = []): array
    {
        // Only get the an interngy intensity from GWF if one is not provided
        if(empty($attributes['co2Intensity'])) {
            $attributes['co2Intensity'] = self::co2Intensity($url);
        }

        // Process the attributes to get the defaults
        $attributes = self::attributes($attributes);

        return self::breakdown(
            self::bytesFromGPSAPI($url, $attributes),
            $attributes,
        );
    }

    public static function urlWithBytes(string $url, int $bytes, array $attributes = []): array
    {
        // Only get the an interngy intensity from GWF if one is not provided
        if (empty($attributes['co2Intensity'])) {
            $attributes['co2Intensity'] = self::co2Intensity($url);
        }

        // Process the attributes to get the defaults
        $attributes = self::attributes($attributes);

        return self::breakdown(
            $bytes,
            $attributes,
        );
    }

    public static function breakdown(float $bytes, array $attributes = []): array
    {
        $attributes = self::attributes($attributes);

        $gCO2e = self::gCO2e($bytes, $attributes);
        $energy = self::energy($bytes);

        $attributes['bytes'] = $bytes;
        $attributes['adjustedBytes'] = self::adjustDataTransfer($bytes);

        return [
            'gCO2e' => $gCO2e,
            'rating' => self::rating($gCO2e),
            'energy' => $energy,
            'segments' => [
                'dataCentre' =>
                    $energy * self::PERCENTAGE_OF_ENERGY_IN_DATACENTER * $attributes['co2Intensity'],
                'transmission' =>
                    $energy * self::PERCENTAGE_OF_ENERGY_IN_TRANSMISSION * self::GLOBAL_AVERAGE_CO2_INTENSITY_GRID,
                'endUserDevices' =>
                    $energy * self::PERCENTAGE_OF_ENERGY_IN_END_USER_DEVICES * self::GLOBAL_AVERAGE_CO2_INTENSITY_GRID,
                'hardwareProduction' =>
                    $energy * self::PERCENTAGE_OF_ENERGY_IN_HARDWARE_PRODUCTION * self::GLOBAL_AVERAGE_CO2_INTENSITY_GRID,
            ],
            'variables' => $attributes,
        ];
    }

    public static function gCO2e(float $bytes, array $attributes = []): float
    {
        $attributes = self::attributes($attributes);
        $energy = $attributes['adjustBytesTransfered']
            ? self::energy(self::adjustDataTransfer($bytes))
            : self::energy($bytes);

        return (($energy * self::PERCENTAGE_OF_ENERGY_IN_DATACENTER) * $attributes['co2Intensity']) +
            (
                (
                    $energy * (
                        self::PERCENTAGE_OF_ENERGY_IN_TRANSMISSION +
                        self::PERCENTAGE_OF_ENERGY_IN_END_USER_DEVICES +
                        self::PERCENTAGE_OF_ENERGY_IN_HARDWARE_PRODUCTION
                    )
                ) * self::GLOBAL_AVERAGE_CO2_INTENSITY_GRID
            );
    }

    public static function rating(float $gCO2e): string
    {
        // Should probably define these somewhere else and explain what they are
        if ($gCO2e < 0.09527737) {
            return 'A+';
        } elseif ($gCO2e < 0.18570539) {
            return 'A';
        } elseif ($gCO2e < 0.34118537) {
            return 'B';
        } elseif ($gCO2e < 0.49311311) {
            return 'C';
        } elseif ($gCO2e < 0.65555770) {
            return 'D';
        } elseif ($gCO2e < 0.84594812) {
            return 'E';
        }

        return 'F';
    }

    private static function co2Intensity(string $url): float
    {
        $data = self::ipToCO2Intensity($url);

        if (is_float($data->carbon_intensity)) {
            return $data->carbon_intensity;
        }

        $data = self::greenCheck($url);

        return property_exists($data, 'green') && $data->green === true
            ? self::GLOBAL_AVERAGE_CO2_INTENSITY_RENEWABLE
            : self::GLOBAL_AVERAGE_CO2_INTENSITY_GRID;
    }

    private static function adjustDataTransfer(float $bytes): float
    {
        $newVisitorPercentage = 0.75;
        $oldVisitorPercentage = 1 - $newVisitorPercentage;
        $percentageLoadedOnSubsequentLoad = 0.02;

        return ($bytes * $newVisitorPercentage) + ($percentageLoadedOnSubsequentLoad * $bytes * $oldVisitorPercentage);
    }

    private static function energy(float $bytes): float
    {
        return self::bytesToGB($bytes) * self::KWH_PER_GB;
    }

    private static function attributes(array $attributes = []): array
    {
        return array_merge([
            'adjustBytesTransfered' => true,
            'co2Intensity' => self::GLOBAL_AVERAGE_CO2_INTENSITY_GRID,
        ], $attributes);
    }
}
