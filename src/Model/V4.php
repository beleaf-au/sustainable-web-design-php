<?php

namespace Beleaf\SustainableWebDesign\Model;

// https://sustainablewebdesign.org/estimating-digital-emissions/

class V4 extends AbstractModel
{
    // Typed class constants is an 8.3 feature. Lets not push too far...
    public const GLOBAL_AVERAGE_CO2_INTENSITY = 494;
    public const KWH_PER_GB_OE_DATA_CENTRE = 0.055;
    public const KWH_PER_GB_OE_NETWORK = 0.059;
    public const KWH_PER_GB_OE_USER_DEVICES = 0.080;
    public const KWH_PER_GB_EE_DATA_CENTRE = 0.012;
    public const KWH_PER_GB_EE_NETWORK = 0.013;
    public const KWH_PER_GB_EE_USER_DEVICES = 0.081;

    public static function url(string $url, array $attributes = []): array
    {
        // No need to query make the api query if the values are provided
        if(empty($attributes['operationalEnergyIntensity']) || empty($attributes['greenHostingFactor'])) {
            // By placing the detect first, we allow for the user to provide the
            // energy properties without them being overwritten.
            $attributes = array_merge(self::detectEnergyProperties($url), $attributes);
        }

        // Process the attributes to get the defaults
        $attributes = self::attributes($attributes);

        return self::breakdown(
            self::bytesFromGPSAPI($url, $attributes),
            $attributes,
        );
    }

    public static function urlWithBytes(string $url, int $bytes, array $attributes = []): array
    {
        // No need to query make the api query if the values are provided
        if (empty($attributes['operationalEnergyIntensity']) || empty($attributes['greenHostingFactor'])) {
            // By placing the detect first, we allow for the user to provide the
            // energy properties without them being overwritten.
            $attributes = array_merge(self::detectEnergyProperties($url), $attributes);
        }

        // Process the attributes to get the defaults
        $attributes = self::attributes($attributes);

        return self::breakdown(
            $bytes,
            $attributes,
        );
    }

    public static function breakdown(float $bytes, array $attributes = []): array
    {
        $attributes = self::attributes($attributes);
        $gCO2e = self::gCO2e($bytes, $attributes);

        $attributes['bytes'] = $bytes;

        return [
            'gCO2e' => $gCO2e,
            'rating' => self::rating($gCO2e),
            'segments' => [
                'operationalTotal' => self::operationalEmissions($bytes, $attributes['operationalEnergyIntensity']),
                'embodiedTotal' => self::embodiedEmissions($bytes, $attributes['embodiedEnergyIntensity']),
                'operational' => [
                    'dataCentre' => self::operationalEmissionsDataCentre($bytes, $attributes['operationalEnergyIntensity']),
                    'network' => self::operationalEmissionsNetworks($bytes, $attributes['operationalEnergyIntensity']),
                    'userDevice' => self::operationalEmissionsUserDevices($bytes, $attributes['operationalEnergyIntensity']),
                ],
                'embodied' => [
                    'dataCentre' => self::embodiedEmissionsDataCentre($bytes, $attributes['embodiedEnergyIntensity']),
                    'network' => self::embodiedEmissionsNetworks($bytes, $attributes['embodiedEnergyIntensity']),
                    'userDevice' => self::embodiedEmissionsUserDevices($bytes, $attributes['embodiedEnergyIntensity']),
                ],
            ],
            'variables' => $attributes,
        ];
    }

    public static function gCO2e(float $bytes, array $attributes = []): float
    {
        $attributes = self::attributes($attributes);

        $opDC = self::operationalEmissionsDataCentre($bytes, $attributes['operationalEnergyIntensity']);
        $opN = self::operationalEmissionsNetworks($bytes, $attributes['operationalEnergyIntensity']);
        $opUD = self::operationalEmissionsUserDevices($bytes, $attributes['operationalEnergyIntensity']);

        $emDC = self::embodiedEmissionsDataCentre($bytes, $attributes['embodiedEnergyIntensity']);
        $emN = self::embodiedEmissionsNetworks($bytes, $attributes['embodiedEnergyIntensity']);
        $emUD = self::embodiedEmissionsUserDevices($bytes, $attributes['embodiedEnergyIntensity']);

        $newVisitorRatio = 1 - $attributes['returnVisitorRatio'];
        $nonGreenHostingFactor = 1 - $attributes['greenHostingFactor'];
        $nonCachedDataRatio = 1 - $attributes['dataCacheRatio'];

        return (
            (
                ($opDC * $nonGreenHostingFactor + $emDC) +
                ($opN + $emN) + ($opUD + $emUD)
            ) * $newVisitorRatio
        ) +
            (
                (
                    ($opDC * $nonGreenHostingFactor +  $emDC) +
                    ($opN + $emN) + ($opUD + $emUD)
                ) * $attributes['returnVisitorRatio'] * $nonCachedDataRatio
            );
    }

    public static function rating(float $gCO2e): string
    {
        // Should probably define these somewhere else and explain what they are
        if ($gCO2e < 0.040385982) {
            return 'A+';
        } elseif ($gCO2e < 0.07871643) {
            return 'A';
        } elseif ($gCO2e < 0.14462097) {
            return 'B';
        } elseif ($gCO2e < 0.209019798) {
            return 'C';
        } elseif ($gCO2e < 0.277876482) {
            return 'D';
        } elseif ($gCO2e < 0.358578792) {
            return 'E';
        }

        return 'F';
    }

    private static function detectEnergyProperties(string $url): array
    {
        $data = self::ipToCO2Intensity($url);

        $energyProperties = [];

        if (is_float($data->carbon_intensity)) {
            $energyProperties['operationalEnergyIntensity'] = $data->carbon_intensity;
        }

        /**
         * generation from fossil is a number between 0 and 100, possibly with
         * decimal places
         */
        if (is_float($data->generation_from_fossil)) {
            $energyProperties['greenHostingFactor'] = 1 - $data->generation_from_fossil / 100;
        }

        return $energyProperties;
    }

    private static function operationalEmissions(float $bytes, float $operationalEnergyIntensity = self::GLOBAL_AVERAGE_CO2_INTENSITY): float
    {
        return self::operationalEmissionsDataCentre($bytes, $operationalEnergyIntensity)
            + self::operationalEmissionsNetworks($bytes, $operationalEnergyIntensity)
            + self::operationalEmissionsUserDevices($bytes, $operationalEnergyIntensity);
    }

    private static function embodiedEmissions(float $bytes): float
    {
        return self::embodiedEmissionsDataCentre($bytes)
            + self::embodiedEmissionsNetworks($bytes)
            + self::embodiedEmissionsUserDevices($bytes);
    }

    private static function operationalEmissionsDataCentre(float $bytes, float $operationalEnergyIntensity = self::GLOBAL_AVERAGE_CO2_INTENSITY): float
    {
        return self::bytesToGB($bytes) * self::KWH_PER_GB_OE_DATA_CENTRE * $operationalEnergyIntensity;
    }

    private static function operationalEmissionsNetworks(float $bytes, float $operationalEnergyIntensity = self::GLOBAL_AVERAGE_CO2_INTENSITY): float
    {
        return self::bytesToGB($bytes) * self::KWH_PER_GB_OE_NETWORK * $operationalEnergyIntensity;
    }

    private static function operationalEmissionsUserDevices(float $bytes, float $operationalEnergyIntensity = self::GLOBAL_AVERAGE_CO2_INTENSITY): float
    {
        return self::bytesToGB($bytes) * self::KWH_PER_GB_OE_USER_DEVICES * $operationalEnergyIntensity;
    }

    private static function embodiedEmissionsDataCentre(float $bytes, $embodiedEnergyIntensity = self::GLOBAL_AVERAGE_CO2_INTENSITY): float
    {
        return self::bytesToGB($bytes) * self::KWH_PER_GB_EE_DATA_CENTRE * $embodiedEnergyIntensity;
    }

    private static function embodiedEmissionsNetworks(float $bytes, $embodiedEnergyIntensity = self::GLOBAL_AVERAGE_CO2_INTENSITY): float
    {
        return self::bytesToGB($bytes) * self::KWH_PER_GB_OE_NETWORK * $embodiedEnergyIntensity;
    }

    private static function embodiedEmissionsUserDevices(float $bytes, $embodiedEnergyIntensity = self::GLOBAL_AVERAGE_CO2_INTENSITY): float
    {
        return self::bytesToGB($bytes) * self::KWH_PER_GB_OE_USER_DEVICES * $embodiedEnergyIntensity;
    }

    private static function attributes(array $attributes = []): array
    {
        return array_merge([
            /**
             * If you know the carbon intensity of the electricity grid in which
             * your data center, network, or user devices are located, you
             * should adjust the grid intensity to estimate the operational
             * emissions of that segment. This will give you results that are
             * more representative of how your website is operated and used.
             */
            'operationalEnergyIntensity' => self::GLOBAL_AVERAGE_CO2_INTENSITY,
            /**
             * We do not recommend changing the grid intensity value used for
             * the embodied emissions calculations. This is due to the fact that
             * production of nearly all digital hardware products involve a
             * global supply chain. For this reason, we recommend using the
             * global average grid intensity value.
             */
            'embodiedEnergyIntensity' => self::GLOBAL_AVERAGE_CO2_INTENSITY,
            /**
             * The green hosting factor allows for data center operational
             * emissions to be adjusted for the percentage of hosting that comes
             * from renewable or zero-carbon fuel sources.
             *
             * The ratio is a value from 0 to 1, representing the percentage of
             * hosting services powered by renewable or zero-carbon energy. For
             * instance, you may know that your hosting provider operates in a
             * region where 40% of total electricity production is from
             * renewable sources. In this case, you would use a green hosting
             * factor of 0.4 in your estimates.
             *
             * You can check if your hosting provider is a known, verified
             * green host by using the Green Web Foundation’s online checking
             * tool, or their Greencheck API. If your website returns as hosted
             * green in these checks, then you can use a green hosting factor
             * of 1 in your estimates.
             */
            'greenHostingFactor' => 0,
            /**
             * The return visitor ratio is a value that represents the number
             * of website visitors who have returned to a page. Since many
             * website cache static assets on the visitors device, this data
             * is neither being transferred via the network or computed in a
             * data center. The returning visitor ratio is used alongside the
             * data cache ratio to reflect this behaviour in estimates. We have
             * made the simplifying assumption that a return visitor will
             * consume only energy on its device to handle the non-cached data.
             */
            'dataCacheRatio' => 0,
            'returnVisitorRatio' => 0,
        ], $attributes);
    }
}
